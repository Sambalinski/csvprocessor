package com.example.csvprocessor.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies.UpperCamelCaseStrategy
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class AppConfig {

    @Bean
    fun jacksonObjectMapper(): ObjectMapper =
        ObjectMapper()
            .setPropertyNamingStrategy(UpperCamelCaseStrategy())
}
