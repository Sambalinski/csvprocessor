package com.example.csvprocessor.helper

import org.apache.camel.Handler
import org.springframework.stereotype.Component
import java.util.concurrent.atomic.AtomicInteger

@Component
class CounterService {
    private val counter = AtomicInteger()

    @Handler
    fun count() = counter.incrementAndGet()
}
