package com.example.csvprocessor.dto

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import org.apache.camel.dataformat.bindy.annotation.CsvRecord
import org.apache.camel.dataformat.bindy.annotation.DataField

@JsonPropertyOrder(alphabetic = true)
@CsvRecord(separator = ",", skipField = true)
data class Product(
    @field:DataField(pos = 1)
    val UniqueID: String? = null,
    @field:DataField(pos = 2)
    val ProductCode: String? = null,
    @field:DataField(pos = 3)
    val ProductName: String? = null,
    @field:DataField(pos = 4)
    val PriceWholesale: Double? = null,
    @field:DataField(pos = 5)
    val PriceRetail: Double? = null,
    @field:DataField(pos = 6)
    val InStock: Int? = null,
)
