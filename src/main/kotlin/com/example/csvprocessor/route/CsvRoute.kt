package com.example.csvprocessor.route

import com.example.csvprocessor.dto.Product
import com.example.csvprocessor.helper.CounterService
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat
import org.apache.camel.model.dataformat.JsonLibrary
import org.apache.camel.spi.DataFormat
import org.springframework.stereotype.Component

@Component
class CsvRoute : RouteBuilder() {

    override fun configure() {

        val bindy: DataFormat = BindyCsvDataFormat(Product::class.java)

        from("file:{{csvprocessor.fileexchange.input}}?antInclude=$csvPattern")
            .split(
                body().tokenize()
            )
            .streaming()
            .parallelProcessing()
            .unmarshal(bindy)
            .split(body())
            .marshal().json(JsonLibrary.Jackson)
            .to("activemq:{{csvprocessor.amq.destination}}")
            .bean(CounterService::class.java, "count")
            .log("Обработано строк: \${body}")
    }

    companion object {
        private const val csvPattern = "*.csv"
    }
}
