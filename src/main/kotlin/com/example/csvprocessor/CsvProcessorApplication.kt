package com.example.csvprocessor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CsvProcessorApplication

fun main(args: Array<String>) {
    runApplication<CsvProcessorApplication>(*args)
}
