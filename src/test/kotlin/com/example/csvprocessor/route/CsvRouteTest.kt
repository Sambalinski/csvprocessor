package com.example.csvprocessor.route

import com.example.csvprocessor.TestEntities.CSV_BODY
import com.example.csvprocessor.TestEntities.MESSAGE_BODY
import org.apache.camel.CamelContext
import org.apache.camel.EndpointInject
import org.apache.camel.Produce
import org.apache.camel.ProducerTemplate
import org.apache.camel.ServiceStatus
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.test.spring.junit5.CamelSpringBootTest
import org.apache.camel.test.spring.junit5.MockEndpoints
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTestContextBootstrapper
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD
import org.springframework.test.context.BootstrapWith
import org.springframework.test.context.ContextConfiguration
import java.io.File


@CamelSpringBootTest
@ContextConfiguration
@BootstrapWith(SpringBootTestContextBootstrapper::class)
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@MockEndpoints("activemq:products")
class CsvRouteTest {

    @Autowired
    lateinit var camelContext: CamelContext

    @EndpointInject(uri = "mock:activemq:{{csvprocessor.amq.destination}}")
    lateinit var mockActiveMq: MockEndpoint

    @Produce(uri = "file:{{csvprocessor.fileexchange.input}}?filename=test.csv")
    lateinit var start: ProducerTemplate

    @Test
    fun `should process csv file`() {
        assertEquals(ServiceStatus.Started, camelContext.status)

        mockActiveMq.expectedBodiesReceived(MESSAGE_BODY)

        start.sendBody(CSV_BODY)

        mockActiveMq.assertIsSatisfied()

    }

    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    companion object {
        @AfterAll
        @JvmStatic
        fun `clean csvinput directory`() {
            File("src/test/resources/csvinput/.camel/").deleteRecursively()
        }
    }
}
