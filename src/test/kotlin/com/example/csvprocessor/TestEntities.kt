package com.example.csvprocessor

object TestEntities {
    const val CSV_BODY = "\"b07243e1-bf23-4017-8e19-4ac863cd5ee1\",\"04443-22491\",\"BOOSTER KIT\",112.50,126.85,201468"
    const val MESSAGE_BODY =
        "{\"InStock\":201468,\"PriceRetail\":126.85,\"PriceWholesale\":112.5,\"ProductCode\":\"04443-22491\",\"ProductName\":\"BOOSTER KIT\",\"UniqueID\":\"b07243e1-bf23-4017-8e19-4ac863cd5ee1\"}"
}
